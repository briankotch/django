from tastypie.resources import ModelResource
from tastypie.constants import ALL
from tastypie import fields
from clariture.models import Test, Question, Choice

class TestResource(ModelResource):
    class Meta:
        queryset = Test.objects.all()
        resource_name = 'test'
    
class ChoiceResource(ModelResource):
    class Meta:
        queryset = Choice.objects.all()

class QuestionResource(ModelResource):
    choices = fields.ToManyField(ChoiceResource, 'choice_set', full=True)
    class Meta:
        queryset = Question.objects.all().order_by('?')
        resource_name = 'question'
