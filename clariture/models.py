from django.db import models
class Test(models.Model):
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name

class Question(models.Model):
    question_text = models.CharField(max_length=200);
    def __str__(self):
        return self.question_text
    
class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __str__(self):
        return self.choice_text
