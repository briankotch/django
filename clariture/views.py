from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from clariture.models import Question, Choice
def index(request):
    question = Question.objects.all().order_by('?')[0]
    choices = question.choice_set.all()
    context = {'question': question, 'choices':choices}
    return render(request, 'index.html', context)
