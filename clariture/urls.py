from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from tastypie.api import Api
from clariture.api import *
from clariture import views

v1_api = Api(api_name='v1')
v1_api.register(TestResource())
v1_api.register(QuestionResource())
v1_api.register(ChoiceResource())

urlpatterns = patterns('',
                       url(r'^api/', include(v1_api.urls)),
                       url(r'^$', views.index, name='index')
)
